
# svd_to_memory_map

Headers generation example

```bash
python3.7 tools/memory_map_gen/main.py --directory include/memory_map/
```

Use examples are on branches:
 - ltyrala/nucleo-f303re

## Implemented Enhancements
 - Generate memory map C++ headers from ARM SVD file
 - Compile time access protection
 - Easy to use, easy to understand
 - Without unions for bit access (eliminared undefined behaviour)

## License

svd_to_memory_map is licensed under Apache-2.0.
