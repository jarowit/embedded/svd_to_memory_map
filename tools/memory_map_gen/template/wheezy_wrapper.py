#!/usr/bin/python3
# Copyright 2018 Lukasz Tyrala <ltyrala83@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import argparse
from builtins import str

from wheezy.template.engine import Engine
from wheezy.template.ext.code import CodeExtension
from wheezy.template.ext.core import CoreExtension
from wheezy.template.loader import FileLoader 


class WheezyWrapper:
    def __init__(self):
        self._template_dir = ""
        self._template_name = ""
        self._result = ""
        self._filter_dict = {}

    def template_dir(self, directory: str):
        self._template_dir = directory

    def template_name(self, file: str):
        self._template_name = file

    def add_filters(self, name, function):
        self._filter_dict[name] = function

    def render(self, contex_dict):
        engine = Engine(loader=FileLoader([self._template_dir]),
                        extensions=[CoreExtension(), CodeExtension()])
        engine.global_vars.update(self._filter_dict)
        template = engine.get_template(self._template_name)
        self._result = template.render(contex_dict)
