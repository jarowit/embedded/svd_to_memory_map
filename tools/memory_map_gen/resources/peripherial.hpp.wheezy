@require(peripheral)
//   Copyright 2018 Lukasz Tyrala <ltyrala83@@gmail.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.


#ifndef SRC_DRIVER_MEMORY_MAP_@{peripheral.name}_HPP_
#define SRC_DRIVER_MEMORY_MAP_@{peripheral.name}_HPP_

#include <cstdint>
#include "bit_field/BitField.hpp"

namespace memory_map {

// @{peripheral.name}: @{ful_strip(peripheral.description)}
struct @{peripheral.name}
{

    @for register in peripheral.registers:
    struct @{register.name.lower()}_t : public RegisterBase<AccessType::@{to_type_enum(register.access)}>
    {
        struct data
        {
            enum
            {
                RESET_MASK = @{hex(register.reset_mask)},
                RESET_VALUE = @{hex(register.reset_value)},
            };
        };

        @for field in register.fields:
        // @{ful_strip(field.description)}
        constexpr WriteBits @{field.name}(std::uint32_t value) { return BitField<@{str(field.bit_offset)}u, @{str(field.bit_width)}u>().value(value); } 
        constexpr ReadBits  @{field.name}() { return ReadBits(BitField<@{str(field.bit_offset)}u, @{str(field.bit_width)}u>()); }

        @end
    };

    @end

public:
@(last_offset = -4)@#
    struct Registers {
    @for i, register in enumerate(peripheral.registers):
        @if (register.address_offset-last_offset) != 4:
@(reserved_size = (register.address_offset-last_offset-4)/4)@#
        std::uint32_t reserved_@{str(i)}[@{str(int(reserved_size))}];
        @endif
        @{register.name.lower()}_t @{register.name};
@(last_offset = register.address_offset)@#
    @end
    };

    Registers& reg{*reinterpret_cast<Registers*>(@{hex(peripheral.base_address)})};
};

} // namespace memory_map

#endif // SRC_DRIVERS_MEMORY_MAP_@{peripheral.name}_HPP_

