#!/usr/bin/python3
# Copyright 2018 Lukasz Tyrala <ltyrala83@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import argparse
from pathlib import Path
from re import sub

from cmsis_svd.parser import SVDParser

from template.wheezy_wrapper import WheezyWrapper


def get_template_dir():
    script_dir = Path(__file__).resolve().parent 
    return f"{script_dir.absolute()}/resources"

def generate(config):
    parser = SVDParser.for_packaged_svd(config.vendor, config.filename)
    device = parser.get_device()

    work_dir = Path(config.directory)
    work_dir.mkdir(parents=True, exist_ok=True)

    for peripheral in device.peripherals:
        wheezy_wrapper = WheezyWrapper()
        wheezy_wrapper.add_filters("ful_strip", lambda s: sub("\n|\r|\s+", " ", s))
        wheezy_wrapper.add_filters("to_type_enum", lambda s: sub("-", "_", s or "read-write").upper())
        wheezy_wrapper.template_dir(get_template_dir())
        wheezy_wrapper.template_name("peripherial.hpp.wheezy")
        wheezy_wrapper.render({"peripheral": peripheral})

        file_name = f"{peripheral.name.lower()}_base.hpp"
        file_path = work_dir / file_name
        with file_path.open("w", encoding ="utf-8") as f:
            f.write(wheezy_wrapper._result)

def main():
    parser = argparse.ArgumentParser("Header files generator. SDV xml file to c++ Header.\n")
    parser.add_argument("--directory",
                        dest="directory",
                        default="memory_map",
                        help="where to save generated files, defalut is ./memory_map")
    parser.add_argument("--vendor",
                        dest="vendor",
                        default="STMicro",
                        help="Vendor directory name, defalut: STMicro")
    parser.add_argument("--filename",
                        dest="filename",
                        default="STM32F303.svd",
                        help="Svd file name, defalut: STM32F303.svd")
    config = parser.parse_args()

    generate(config)

if __name__ == '__main__':
    main()
