//   Copyright 2018 Lukasz Tyrala <ltyrala83@gmail.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef TOOLS_MEMORY_MAP_GEN_RESOURCES_BITFIELD_HPP_
#define TOOLS_MEMORY_MAP_GEN_RESOURCES_BITFIELD_HPP_

#include <cstdint>
#include <limits>

namespace memory_map {

struct AccessType
{
    enum access_type_
    {
        READ_WRITE,
        READ_ONLY,
        WRITE_ONLY,
    };
};

struct ReadBits
{
    std::uint32_t offset;
    std::uint32_t mask;
};

struct WriteBits
{
    std::uint32_t value;
    std::uint32_t mask;

    constexpr WriteBits operator|(const WriteBits& rhs) const
    {
        return WriteBits{.value=value|rhs.value, .mask=mask|rhs.mask};
    }
};

template<std::uint32_t offset, std::uint32_t bit_width>
class BitField
{
    enum
    {
        MAX = (bit_width>31) ? std::numeric_limits<std::uint32_t>::max() : (1u<<bit_width)-1,
        MASK = MAX << offset,
        OFFSET = offset,
    };
public:
    constexpr operator ReadBits() const
    {
        return ReadBits{.offset=OFFSET, .mask=MASK};
    }

    constexpr WriteBits value(std::uint32_t value)
    {
        return WriteBits{.value=value<<OFFSET, .mask=MASK};
    }
};

template<AccessType::access_type_ type = AccessType::READ_WRITE>
struct RegisterBase
{
    void set(const WriteBits& bits)
    {
        m_value = (m_value & ~bits.mask) | bits.value;
    }

    std::uint32_t get(const ReadBits& bits) const
    {
        return (m_value & bits.mask) >> bits.offset;
    }

    volatile std::uint32_t m_value;
};

template<>
struct RegisterBase<AccessType::WRITE_ONLY>
{
    void set(const WriteBits& bits)
    {
        m_value = (m_value & ~bits.mask) | bits.value;
    }

    volatile std::uint32_t m_value;
};

template<>
struct RegisterBase<AccessType::READ_ONLY>
{
    std::uint32_t get(const ReadBits& bits) const
    {
        return (m_value & bits.mask) >> bits.offset;
    }

    volatile std::uint32_t m_value;
};

}  // namespace memory_map


#endif /* TOOLS_MEMORY_MAP_GEN_RESOURCES_BITFIELD_HPP_ */
